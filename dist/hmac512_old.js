"use strict";

var R = require("ramda");

var BigNumber = require("bignumber.js");

var SHA512 = require("sha512sha512"); //For easy way


var print = console.log;

var bn = function bn(x) {
  return new BigNumber(x);
};

var bn2 = function bn2(x) {
  return new BigNumber(x, 2);
};

var bn16 = function bn16(x) {
  return new BigNumber(x, 16);
}; //Support functions


var bin2bin64 = R.pipe(function (x) {
  return x.split("");
}, function (x) {
  return R.repeat("0", 64 - x.length).concat(x);
}); //print(bin2bin64("1"), bin2bin64("1").length);

var bin2hex = R.pipe(function (x) {
  return x.join("");
}, function (x) {
  return parseInt(x, 2);
}); //print(bin2hex(["1", "0", "0", "0", "0", "0", "0", "1"]));

var bn2str = R.pipe(R.map(function (x) {
  return x.toString(2);
}), R.map(bin2bin64), R.map(R.splitEvery(8)), R.map(R.map(bin2hex)), R.reduce(R.concat, []), R.map(String.fromCharCode), R.reduce(function (x, y) {
  return x + y;
}, "")); //print(bn2str(SHA512.SHA512("abc")), bn2str(SHA512.SHA512("abc")).length);
//print(String.fromCharCode(97).charCodeAt(0));

var key2hash = function key2hash(x, y) {
  return x.length * 8 > y ? bn2str(SHA512.SHA512(x)) : x;
}; //print(key2hash("abc", 33));
//Input setting


var bin2byte = function bin2byte(x) {
  return R.repeat("0", 8 - x.length).concat(x);
};

var bin2qword = function bin2qword(x) {
  return R.repeat("0", 64 - x.length).concat(x);
};

var sum = R.curry(function (x, y) {
  return x + y;
});
var str2bin = R.pipe(R.split(""), R.map(function (x) {
  return x.charCodeAt(0);
}), R.map(bn), R.map(function (x) {
  return x.toString(2);
}), R.map(R.split("")), R.map(bin2byte), R.reduce(R.concat, [])); //print(str2bin("abc"), str2bin("abc").length);

var bn2qword = R.pipe(function (x) {
  return x.toString(2);
}, R.split(""), bin2qword);
var sha2bin512 = R.pipe(R.map(bn2qword), R.reduce(R.concat, [])); //print(sha2bin512(SHA512.SHA512("abc")), sha2bin512(SHA512.SHA512("abc")).length);

var padKey = function padKey(x, y) {
  return x.concat(R.repeat("0", y - x.length));
};

var keyLonger = R.curry(function (x, y) {
  return y.length * 8 > x ? sha2bin512(SHA512.SHA512(y)) : str2bin(y);
}); //print(keyLonger("abc", 3));

var keyShorter = R.curry(function (x, y) {
  return y.length < x ? padKey(y, x) : y;
});
var stateValue = R.curry(function (x, y) {
  return R.pipe(function (z) {
    return z / 8;
  }, //R.tap(print),
  R.repeat(bin2byte(bn16(x).toString(2).split(""))), //R.tap(print),
  R.reduce(R.concat, []) //bn,
  //z => z.multipliedBy(bn(x)),
  //z => z.toString(2),
  //R.split(""),
  //z => R.repeat("0", 1024 - z.length).concat(z)
  )(y);
}); //print(stateValue("0x5c")(1));

var binXor = R.curry(function (x, y) {
  return R.pipe(R.zip(y), R.map(function (z) {
    return z[0] === z[1] ? "0" : "1";
  }))(x);
}); //print(binXor(["1", "0", "0"], ["1", "0", "1"]));

var outerKey = function outerKey(x, y) {
  return R.pipe(stateValue("5c"), //R.tap(z => print("outerKey {1} :", z.join(""), z.join("").length)),
  //R.tap(z => print("outerKey {2} :", x.join(""), x.join("").length)),
  binXor(x) //R.tap(x => print(bn2(x.join("")).valueOf()))
  )(y);
};

var innerKey = function innerKey(x, y) {
  return R.pipe(stateValue("36"), //R.tap(x => print(bn2(x.join("")).valueOf())),
  //R.tap(z => print(x)),
  binXor(x))(y);
}; //Main functions


var key2str = R.pipe(R.splitEvery(64), //R.tap(z => print(z)),
R.map(R.join("")), //R.tap(z => print(z)),
R.map(bn2), //R.tap(z => print(z)),
bn2str //R.tap(z => print(z)),
);

var str2byteArray = function str2byteArray(x) {
  return x.split("").map(function (s) {
    return bn(s.charCodeAt(0)).toString(16);
  });
};

var result2byteArray = R.pipe(R.map(function (x) {
  return x.toString(16);
}), R.map(R.splitEvery(2)), R.reduce(R.concat, []));
var key2byteArray = R.pipe(R.splitEvery(8), R.map(R.join("")), R.map(bn2), R.map(function (x) {
  return x.toString(16);
}), R.map(function (x) {
  return R.repeat("0", 2 - x.length) + x;
}));

var hmac = function hmac(key, message, blockSize) {
  return R.pipe(keyLonger(blockSize), //R.tap(x => print(x.join(""), x.join("").length)),
  keyShorter(blockSize), //R.tap(x => print(x.join(""), x.join("").length)),
  function (x) {
    return [outerKey(x, blockSize), innerKey(x, blockSize)];
  }, //R.tap(x => print("hmac {3} :", x[0].join(""), x[0].join("").length)),
  //R.tap(x => print("hmac {4} :", x[1].join(""), x[1].join("").length)),
  R.map(key2byteArray), //R.tap(x => print("ХУЙ", x[1] + message)),
  //R.tap(x => print(sum(x[1], message).split("").map(s => s.charCodeAt(0)))),
  //R.map(str2byteArray),
  //R.map(print),
  //R.tap(x => print(x[1].split("").map(s => bn(s.charCodeAt(0)).toString(16) ).join("") )),
  //R.tap(x => print(sum(x[1], message).split("").map(s => bn(s.charCodeAt(0)).toString(16) ).join("") )),
  //R.tap(x => print(x[1], x[1].length)),
  //R.tap(x => print(x[1].concat(str2byteArray(message)))),
  //x => SHA512.SHA512(x[1].concat(str2byteArray(message))),
  //R.tap(x => print(result2byteArray(SHA512.SHA512(x[1].concat(str2byteArray(message)))), result2byteArray(SHA512.SHA512(x[1].concat(str2byteArray(message)))).length)),
  function (x) {
    return SHA512.SHA512(x[0].concat(result2byteArray(SHA512.SHA512(x[1].concat(str2byteArray(message))))));
  } //x => result2byteArray(SHA512.SHA512(x[1].concat(str2byteArray(message))))
  )(key);
}; //print(bn2("101").valueOf());


var key1 = R.repeat(String.fromCharCode(0x0b), 20).join("");
var key2 = R.repeat(String.fromCharCode(0xaa), 20).join("");
var text2 = R.repeat(String.fromCharCode(0xdd), 50).join("");
var key3 = R.range(1, 26).map(function (x) {
  return String.fromCharCode(x);
}).join("");
var text3 = R.repeat(String.fromCharCode(0xcd), 50).join("");
var key4 = R.repeat(String.fromCharCode(0xaa), 131).join("");
var text4 = "Test Using Larger Than Block-Size Key - Hash Key First";
var key5 = R.repeat(String.fromCharCode(0xaa), 131).join("");
var text5 = "This is a test using a larger than block-size key and a larger than block-size data. The key needs to be hashed before being used by the HMAC algorithm."; //SHA512.result2print(hmac(key, "Hi There", 1024));
//SHA512.result2print(hmac("", "", 1024));
//print(parseInt("ff", 16));
//SHA512.result2print(hmac("Jefe", "what do ya want for nothing?", 1024));
//SHA512.result2print(hmac(key1, "Hi There", 1024));
//SHA512.result2print(hmac(key2, text2, 1024));
//SHA512.result2print(hmac(key3, text3, 1024));
//SHA512.result2print(hmac(key4, text4, 1024));

SHA512.result2print(hmac(key5, text5, 1024));
var test = [bn16("d392b9a631704237"), bn16("c931e042fd4fd9bd"), bn16("8ecf25e9566fd1d0"), bn16("b1d094291629e0ee"), bn16("90ac01da30011558"), bn16("7749951e5ca377dd"), bn16("410a6c9eff5ec733"), bn16("1494f3f9cc1cc8a6")]; //print(result2byteArray(test));